import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let maxHPfirst = firstFighter.health;
    let maxHPsecond = secondFighter.health;

    firstFighter.cooldown = 0;
    secondFighter.cooldown = 0;

    const Keys = new Map();

    document.addEventListener('keydown', (event) => {
      Keys.set(event.code, true);

      FightCheck(firstFighter, secondFighter, Keys, maxHPfirst, maxHPsecond);

      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }
      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      Keys.delete(event.code);
    });
  });
}

function FightCheck(firstFighter, secondFighter, keys, max1, max2) {
  const indicator1 = document.getElementById('left-fighter-indicator');
  const indicator2 = document.getElementById('right-fighter-indicator');

  switch (true) {
    case keys.has(controls.PlayerOneAttack):
      makeAttack(firstFighter, secondFighter, max2, indicator2, keys);
      break;

    case keys.has(controls.PlayerTwoAttack):
      makeAttack(secondFighter, firstFighter, max1, indicator1, keys);
      break;

    case controls.PlayerOneCriticalHitCombination.every(key => keys.has(key)):
      MakeAPunch(firstFighter, secondFighter, max2, indicator2);
      break;

    case controls.PlayerTwoCriticalHitCombination.every(key => keys.has(key)):
      MakeAPunch(secondFighter, firstFighter, max1, indicator1);
      break;

  };
}

function makeAttack(attacker, defender, maxhp, indicator, keys) {
  if (!isBlocking(keys)) {
    const damage = getDamage(attacker, defender);
    defender.health -= damage;
    updateindicator(defender, indicator, maxhp);
  } else {
    return 0;
  }
}

function updateindicator(defender, indicator, maxhp) {
  const current = defender.health / maxhp;
  const lenth = Math.max(0, (current * 100));
  indicator.style.width = lenth + '%';
}

function isBlocking(keys) {
  if (keys.has(controls.PlayerOneBlock) || keys.has(controls.PlayerTwoBlock)) return true;
  return false;
}

//робить супер удар
function MakeAPunch(attacker, defender, max, indicator) {
  if (isAvailiblePunch(attacker)) {
    defender.health -= attacker.attack * 2.0;
    updateindicator(defender, indicator, max);
    attacker.cooldown = new Date();
  }
}

function isAvailiblePunch(fighter) {
  let now = new Date();
  let distance = Math.abs((now - fighter.cooldown) / 1000);
  if (distance > 10) return true;
  return false;
}

//розраховує урон удару
export function getDamage(attacker, defender) {
  // return damage
  let Hit = getHitPower(attacker);
  let Block = getBlockPower(defender);
  let Cleardamage = Hit - Block;
  return Math.max(0, Cleardamage);
}

//розраховує прямий урон
export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = randomNum();
  let power = fighter.attack * criticalHitChance;
  return power;
}

//розраховує блок при ударі противника
export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = randomNum();
  let power = fighter.defense * dodgeChance;
  return power;
}

//формула яка повертає рандомне число від 1 до 2
export function randomNum() {
  return Math.random() + 1;
}