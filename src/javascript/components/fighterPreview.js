import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const FIGHTimage = createFighterImage(fighter);
    const FIGHTtext = createFighterInfo(fighter);

    fighterElement.appendChild(FIGHTimage);
    fighterElement.appendChild(FIGHTtext);
  }
  //DONE
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;

  const infoElement = createElement({
    tagName: 'p',
    className: 'fighter-preview___root'
  });

  infoElement.innerHTML = `NAME: ${name} HEALTH: ${health} ATTACK: ${attack} DEFENSE: ${defense}`;

  return infoElement;
}